#!/bin/bash

if [ ! -n "$1" ] ; then
    echo Usage: $0 path_to_process
    exit 1
fi

find $1 -type f -not -name \*.xml\* -exec mv \{} \{}.xml \;
for f in `grep -r -l 'href *= *".\+#.*"' $1` ; do
    mv $f $f~
    sed 's/\(href *= *".\+\)\(#.*"\)/\1.xml\2/' $f~ > $f
    rm -f $f~
done
