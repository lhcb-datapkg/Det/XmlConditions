#!/bin/bash

if [ ! -n "$1" ] ; then
    echo Usage: $0 path_to_process
    exit 1
fi

for f in `find $1 -type f -name \*.xml` ; do
    mv $f ${f%.xml}
done

for f in `grep -r -l 'href *= *".\+\.xml#.*"' $1` ; do
    mv $f $f~
    sed 's/\(href *= *".\+\)\.xml\(#.*"\)/\1\2/' $f~ > $f
    rm -f $f~
done
